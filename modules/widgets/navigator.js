/*\
title: $:/core/modules/widgets/navigator.js
type: application/javascript
module-type: widget

Navigator widget

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var IMPORT_TITLE = "$:/Import";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var NavigatorWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
	this.addEventListeners([
		{type: "tm-navigate", handler: "handleNavigateEvent"},
		{type: "tm-close-tiddler", handler: "handleCloseTiddlerEvent"},
		{type: "tm-close-all-tiddlers", handler: "handleCloseAllTiddlersEvent"},
		{type: "tm-close-other-tiddlers", handler: "handleCloseOtherTiddlersEvent"},
		{type: "tm-fold-tiddler", handler: "handleFoldTiddlerEvent"},
		{type: "tm-fold-other-tiddlers", handler: "handleFoldOtherTiddlersEvent"},
		{type: "tm-fold-all-tiddlers", handler: "handleFoldAllTiddlersEvent"},
		{type: "tm-unfold-all-tiddlers", handler: "handleUnfoldAllTiddlersEvent"}
	]);
};

/*
Inherit from the base widget class
*/
NavigatorWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
NavigatorWidget.prototype.render = function(parent,nextSibling) {
	this.parentDomNode = parent;
	this.computeAttributes();
	this.execute();
	this.renderChildren(parent,nextSibling);
};

/*
Compute the internal state of the widget
*/
NavigatorWidget.prototype.execute = function() {
	// Get our parameters
	this.storyTitle = this.getAttribute("story");
	this.historyTitle = this.getAttribute("history");
	this.setVariable("tv-story-list",this.storyTitle);
	this.setVariable("tv-history-list",this.historyTitle);
	// Construct the child widgets
	this.makeChildWidgets();
};

/*
Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
*/
NavigatorWidget.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	if(changedAttributes.story || changedAttributes.history) {
		this.refreshSelf();
		return true;
	} else {
		return this.refreshChildren(changedTiddlers);
	}
};

NavigatorWidget.prototype.getStoryList = function() {
	return this.storyTitle ? this.wiki.getTiddlerList(this.storyTitle) : null;
};

NavigatorWidget.prototype.saveStoryList = function(storyList) {
	if(this.storyTitle) {
		var storyTiddler = this.wiki.getTiddler(this.storyTitle);
		this.wiki.addTiddler(new $tw.Tiddler(
			{title: this.storyTitle},
			storyTiddler,
			{list: storyList}
		));
	}
};

NavigatorWidget.prototype.removeTitleFromStory = function(storyList,title) {
	if(storyList) {
		var p = storyList.indexOf(title);
		while(p !== -1) {
			storyList.splice(p,1);
			p = storyList.indexOf(title);
		}
	}
};

NavigatorWidget.prototype.replaceFirstTitleInStory = function(storyList,oldTitle,newTitle) {
	if(storyList) {
		var pos = storyList.indexOf(oldTitle);
		if(pos !== -1) {
			storyList[pos] = newTitle;
			do {
				pos = storyList.indexOf(oldTitle,pos + 1);
				if(pos !== -1) {
					storyList.splice(pos,1);
				}
			} while(pos !== -1);
		} else {
			storyList.splice(0,0,newTitle);
		}
	}
};

NavigatorWidget.prototype.addToStory = function(title,fromTitle) {
	if(this.storyTitle) {
		this.wiki.addToStory(title,fromTitle,this.storyTitle,{
			openLinkFromInsideRiver: this.getAttribute("openLinkFromInsideRiver","top"),
			openLinkFromOutsideRiver: this.getAttribute("openLinkFromOutsideRiver","top")
		});
	}
};

/*
Add a new record to the top of the history stack
title: a title string or an array of title strings
fromPageRect: page coordinates of the origin of the navigation
*/
NavigatorWidget.prototype.addToHistory = function(title,fromPageRect) {
	this.wiki.addToHistory(title,fromPageRect,this.historyTitle);
};

/*
Handle a tm-navigate event
*/
NavigatorWidget.prototype.handleNavigateEvent = function(event) {
	event = $tw.hooks.invokeHook("th-navigating",event);
	if(event.navigateTo) {
		this.addToStory(event.navigateTo,event.navigateFromTitle);
		if(!event.navigateSuppressNavigation) {
			this.addToHistory(event.navigateTo,event.navigateFromClientRect);
		}
	}
	return false;
};

// Close a specified tiddler
NavigatorWidget.prototype.handleCloseTiddlerEvent = function(event) {
	var title = event.param || event.tiddlerTitle,
		storyList = this.getStoryList();
	// Look for tiddlers with this title to close
	this.removeTitleFromStory(storyList,title);
	this.saveStoryList(storyList);
	return false;
};

// Close all tiddlers
NavigatorWidget.prototype.handleCloseAllTiddlersEvent = function(event) {
	this.saveStoryList([]);
	return false;
};

// Close other tiddlers
NavigatorWidget.prototype.handleCloseOtherTiddlersEvent = function(event) {
	var title = event.param || event.tiddlerTitle;
	this.saveStoryList([title]);
	return false;
};

NavigatorWidget.prototype.handleFoldTiddlerEvent = function(event) {
	var paramObject = event.paramObject || {};
	if(paramObject.foldedState) {
		var foldedState = this.wiki.getTiddlerText(paramObject.foldedState,"show") === "show" ? "hide" : "show";
		this.wiki.setText(paramObject.foldedState,"text",null,foldedState);
	}
};

NavigatorWidget.prototype.handleFoldOtherTiddlersEvent = function(event) {
	var self = this,
		paramObject = event.paramObject || {},
		prefix = paramObject.foldedStatePrefix;
	$tw.utils.each(this.getStoryList(),function(title) {
		self.wiki.setText(prefix + title,"text",null,event.param === title ? "show" : "hide");
	});
};

NavigatorWidget.prototype.handleFoldAllTiddlersEvent = function(event) {
	var self = this,
		paramObject = event.paramObject || {},
		prefix = paramObject.foldedStatePrefix || "$:/state/folded/";
	$tw.utils.each(this.getStoryList(),function(title) {
		self.wiki.setText(prefix + title,"text",null,"hide");
	});
};

NavigatorWidget.prototype.handleUnfoldAllTiddlersEvent = function(event) {
	var self = this,
		paramObject = event.paramObject || {},
		prefix = paramObject.foldedStatePrefix;
	$tw.utils.each(this.getStoryList(),function(title) {
		self.wiki.setText(prefix + title,"text",null,"show");
	});
};

exports.navigator = NavigatorWidget;

})();